// ==UserScript==
// @name email-notifier
// @include https://mail.protonmail.com/*

// @grant GM_xmlhttpRequest
// ==/UserScript==
(function (window, undefined) {
    var w;
    var nameInput = null;
    var telegramInput = null;
    var i = 0;
    var info_div = null;

    const baseurl = 'https://api.telegram.org/bot846769475:AAEGLBp46Bzp7Qp3xoyI9kmcBsxyJRBnbH4/';
    const audiourl = 'https://drive.google.com/uc?export=download&id=1TQj2bpR5mdMWjZ-TMQgDGTV482boKzFl';
    const alteraudiourl = 'https://freesound.org/data/previews/266/266455_4911235-lq.mp3';

        if (typeof unsafeWindow != undefined) {
        w = unsafeWindow
    } else {
        w = window;
    }

    if (w.self != w.top) {
        return;
    }

    var lastmsg_ptid= '---';
    var is_just_loaded = true;

    w.document.body.addEventListener("DOMSubtreeModified", SetListener);
    w.document.body.addEventListener("DOMSubtreeModified", SetNameInput)
    console.log('SET: DOMSubtreeModified');

    function getWrapper() {
        return w.document.getElementsByClassName("conversation-wrapper")[0];
    }


    function SetListener(){
        var wrapper = getWrapper()
        //console.log('CALLED: DOMSubtreeModified');
        if (wrapper != null) {
            wrapper.addEventListener("DOMSubtreeModified", Inserted);
            console.log('SET: DOMNodeInserted');
            w.document.body.removeEventListener("DOMSubtreeModified", SetListener);
            console.log('REMOVED: DOMSubtreeModified');
        }
    }

    function SetNameInput(){
        var leftbar = w.document.getElementById('pm_sidebar');
        var filler = w.document.getElementById('mCSB_2_container_wrapper');
        if (filler != null){
            w.document.body.removeEventListener("DOMSubtreeModified", SetNameInput);

            insert_info();
            var button = w.document.createElement('button');
            button.innerHTML = 'Помощь';
            button.onclick = show_info;
            button.style.margin = "10px auto auto auto";
            button.className="compose pm_button sidebar-btn-compose"
            var describeDiv1 = w.document.createElement('div');
            describeDiv1.style.color = '#ffffffcc';
            describeDiv1.style.margin = "15px 0px 5px 0";
            describeDiv1.style.textAlign = "center";
            describeDiv1.innerHTML = 'на телеграм:';


            telegramInput = w.document.createElement('input');
            telegramInput.id = 'telegram-id';

            leftbar.insertBefore(button, leftbar.children[3])
            leftbar.insertBefore(telegramInput, leftbar.children[3])
            leftbar.insertBefore(describeDiv1, leftbar.children[3])

            var describeDiv2 = w.document.createElement('div');
            describeDiv2.style.color = '#ffffffcc';
            describeDiv2.style.margin = "25px 0px 5px 0";
            describeDiv2.style.textAlign = "center";
            describeDiv2.innerHTML = 'Уведомлять меня только о письмах от:';

            nameInput = w.document.createElement('input');
            nameInput.id = 'notify-name';

            leftbar.insertBefore(nameInput, leftbar.children[3])
            leftbar.insertBefore(describeDiv2, leftbar.children[3])

            console.log('SET: Name input');
        }

    }

    function PlaySound() {
        var audioformsg = new Audio();
        audioformsg.src = alteraudiourl;
        audioformsg.autoplay = true;

        audioformsg.play();
    }

    function SendNotify(message) {
        var chat_id = telegramInput.value;
        console.log('Sending "' + message + '" to ' + chat_id);
        GM_xmlhttpRequest( {method: "GET",
                           url: baseurl + 'sendMessage?chat_id=' + encodeURIComponent(chat_id) + '&text=' +
                           encodeURIComponent(message)} );
    }

    function Inserted(){
        //console.log('CALLED: DOMNodeInserted');
        var wrapper = getWrapper()
        var potential_mail = wrapper.children[0];
        var msg = BuiltMsg(potential_mail);
        //console.log(msg);
        if (potential_mail.getAttribute('data-pt-id') != lastmsg_ptid & msg != null) {
            lastmsg_ptid = potential_mail.getAttribute('data-pt-id');
            if (!is_just_loaded) {
                console.log('MAIL:\ntitle: ' + msg.title + ';\nfrom: ' + msg.from + ';');
                SendNotify('Вы получили сообщение: \nот: ' + msg.from + '\nзаголовок: ' + msg.title);
                //PlaySound();
                //console.log('last message: ' + lastmsg_ptid);
            }
            is_just_loaded = false;
        }

    }

    function BuiltMsg(message_el){
        var wrapper = getWrapper();
        message_el = message_el.lastElementChild;
        var message_el_title = message_el.firstElementChild.firstElementChild;
        var message_el_from = message_el.lastElementChild;
        var message = {'from': '', 'title': ''};
        if (message_el_from != null & message_el_title != null) {
            message.from = message_el_from.innerText.replace(/\r?\n/g, "");
            message.title = message_el_title.innerText.replace(/\r?\n/g, "");
            if (message.title.search((/.*{{.*}}.*/)) == 0) return null;
            if (message.from != nameInput.value && nameInput.value != '') return null;
            return message;
        }
        else return null;
    }

        function insert_info(){
        var content = '<p style="text-align: center; font-size:17px"><strong>E-MAIL NOTIFIER</strong></p><p>Панель управления скриптом расположена с левой стороны страницы. На ней размещены 2 поля: введите в первое фильтр, а во второе ваш телеграм идентификатор.</p><p>Если оставить поле фильтра пустым, вы будете получать уведомления обо всех письмах, приходящих на ящик. Чтобы отключить скрипт, заполните поле фильтра бессмысленным набором символов, например "--".</p><p>Бот не может написать вам, если вы не начнете с ним диалог, поэтому <strong>перед использованием скрипта отправте команду /start <a href="http://t-do.ru/emailnotifier_bot">этому боту</a>.</strong> Свой телеграм идентификатор можно сгенерировать <a href="http://t-do.ru/chatid_echo_bot">вот здесь</a>.</p><p>P.S.: По всем вопросам обращайтесь к разработчику скрипта: neitherone@protonmail.com</p><p style="text-align: center; font-size:17px">🧡</p>'
        var info = w.document.createElement('div');
        info.style.position = "absolute";
        info.style.width = "100%";
        info.style.height = "100%";
        info.style.backgroundColor = "#505061ad";
        info.style.zIndex = "999999";
        info.style.visibility = "hidden";
        info.onclick = hide_info;

        var info_inner = w.document.createElement('div');
        info_inner.style.position = "absolute";
        info_inner.style.width = "520px";
        info_inner.style.height = "auto";
        info_inner.style.left = "0";
        info_inner.style.right = "0";
        info_inner.style.top = "80px";
        info_inner.style.marginLeft = "auto";
        info_inner.style.marginRight = "auto";
        info_inner.style.zIndex = "999999999999999";
        info_inner.style.border = "9px solid #4d4c5d"
        info_inner.style.boxShadow = "0 0 40px #00000054"
        info_inner.style.color = "black";
        info_inner.style.lineHeight = "1.5"
        info_inner.style.textAlign = "justify"
        info_inner.style.backgroundColor = "#f4f4f4";
        info_inner.style.padding = "10px 30px";
        info_inner.innerHTML = content;

        info.appendChild(info_inner)

        w.document.body.insertBefore(info, w.document.body.children[0])

        info_div = info;
    }

    function show_info(){
        info_div.style.visibility = "visible";
    }

    function hide_info(e){
        if (e.target == info_div)
            info_div.style.visibility = "hidden";
    }


})(window);
